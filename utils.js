// 检擦是否支持requestAnimationFrame

var utils = {
    /**
     * 
     * 捕捉鼠标移动的位置
     * 
     * @param {事件对象} element 
     */
    captureMouse: function (element) {
        var mouse = { x: 0, y: 0 }

        element.addEventListener('mousemove', function (event) {
            var x, y
            if (event.pageX || event.pageY) {
                x = event.pageX
                y = event.pageY
            } else {
                x = event.clientX + document.body.scrollLeft + document.documentElement.scrollLeft
                y = event.clientY + document.body.scrollTop + document.documentElement.scrollTop
            }
            x -= element.offsetLeft
            y -= element.offsetTop

            mouse.x = x
            mouse.y = y
        }, false)

        return mouse
    },

    captureTouch: function (element) {
        var touch = { x: null, y: null, isPressed: true }

        element.addEventListener('touchstart', function (event) {
            console.log('touchstart')
            touch.isPressed = true
        }, false)

        element.addEventListener('touchend', function (event) {
            console.log('touchend')
            touch.isPressed = false
            touch.x = null
            touch.y = null
        }, false)

        element.addEventListener('touchmove', function (event) {
            console.log('touchmove')
            var x, y,
                touch_event = event.touches[0];

            if (touch_event.pageX || touch_event.pageY) {
                x = touch_event.pageX
                y = touch_event.pageY
            } else {
                x = touch_event.clientX + document.body.scrollLeft + document.documentElement.scrollLeft
                y = touch_event.clientY + document.body.scrollTop + document.documentElement.scrollTop
            }

            x -= element.offsetLeft
            y -= element.offsetTop

            touch.x = x
            touch.y = y
        }, false)

        return touch
    },

    parseColor: function (color, toNumber) {
        if (toNumber === true) {
            if (typeof color === 'number') {
                return (color | 0)
            }

            if (typeof color === 'string' && color[0] === '#') {
                color = color.slice(1)
            }

            return window.parseInt(color, 16)
        } else {
            if (typeof color === 'number') {
                color = '#' + ('00000' + (color | 0).toString(16)).substr(-6)
            }
            return color
        }
    },

    colortToRGB: function (color, alpha) {
        //if string format convert to number
        if (typeof color === 'string' && color[0] === '#') {
            color = window.parseInt(color.slice(1), 16)
        }
        alpha = (alpha === undefined) ? 1 : alpha

        var r = color >> 16 & 0xff,
            g = color >> 8 & 0xff,
            b = color & 0xff,
            a = (alpha < 0) ? 0 : ((alpha > 1) ? 1 : alpha)
        if (a === 1) {
            return "rgb(" + r + "," + g + "," + b + ")"
        } else {
            return "rgba(" + r + "," + g + "," + b + "," + a + ")"
        }
    }
}