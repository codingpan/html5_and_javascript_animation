# html5_and_javascript_animation

## 常用的一些代码

### 事件创建 检测等

> 检测是否支持canvas

``` javascript
if (document.createElement('canvas').getContext) {
    console.log('support')
}
```

> requestAnimationFrame在各个游览器版本中的实现

``` javascript
if (!window.requestAnimationFrame) {
    window.requestAnimationFrame = (
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        window.oRequestAnimationFrame ||
        window.msRequestAnimationFrame ||
        function(callback) {
            return window.setTimeout(
                callback,
                1000 / 60 //每秒60帧
            )
        }
    )
}
```

> 捕获keydown

``` javascript
textarea.focus()

textarea.addEventListener('keydown', function(event) {
    console.log(event.type)
}, false)
```

## 简单的三角函数概念

![三角形](/img/triangle.png)

正弦 sin : 该角相对的直角边与斜边的比例

余弦 cos : 改角的邻边与斜边的比例

正切 tan : 改角的对边与邻边的比例

反正弦 asin : 正弦的逆运算

反余弦 acos : 余弦的逆运算

反正切 atan : 正切的逆运算

> 角度和弧度的换算

``` javascript
radian = angle * Math.PI / 180
angle = radian * 180 / Math.PI
```

> 通过反三角函数求对应的角度或者弧度

``` javascript
Math.asin(0.5) * 180 / Math.PI //30
Math.sin(30 * Math.PI / 180) //4.999
```

![反三角函数](/img/atan.png)

> 朝鼠标任意一点旋转

``` javascript
dx = mouse.x - obj.x
dy = mouse.y - obj.y
obj.rotation = Math.atan2(dy, dx) //此处是弧度值 
```

> 创建波

``` javascript
function drawFrame() {
    window.requestAnimationFrame(drawFrame, canvas)

    value = cneter + Math.sin(angle) * range
    angle += speed
}
```

> 创建圆形

``` javascript
positionX = centerX + Math.cos(angle) * radius
positionY = centerY + Math.sin(angle) * radius
angle += speed
```

> 创建椭圆形

``` javascript
positionX = centerX + Math.cos(angle) * radiusX
positionY = centerY + Math.sin(angle) * radiusY
angle += speed
```

> 计算两点间距离

``` javascript
dx = x2 - x1
dy = y2 - y1
dist = Math.sqrt(dx * dx + dy * dy)
```

